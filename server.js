/**
 * @fileoverview server entry point
 */

const LOCAL_PORT = 3000;

const express = require('express');
const app = express();

app.use(express.json());

// app.use((req, res, next) => {
//   if (process.env.NODE_ENV === 'production') {
//     if (req.headers['x-forwarded-proto'] !== 'https')
//       return res.redirect('https://' + req.headers.host + req.url);
//     else return next();
//   } else return next();
// });

if (process.env.NODE_ENV === 'production') {
  app.use(express.static('client/dist'));
  const path = require('path');
  app.get('*', (_, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'dist', 'index.html'));
  });
}

const PORT = process.env.PORT || LOCAL_PORT; // use port 0 to take advantage of running tests in parallel

app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}...`);
});

module.exports = app;
