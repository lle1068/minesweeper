import { useEffect } from 'react';
import styled from 'styled-components';
import { Board } from './components/Board';
import { TwinklingStarsBackground } from './components/TwinklingStarsBackground';

const StyledApp = styled.main`
  padding: 4.8rem;
  min-height: calc(100vh - 4.8rem);
  width: 100vw;
  min-width: fit-content;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const StyledHeader = styled.h1`
  font-weight: 400;
  font-size: 8.3rem;
  margin: 0 auto;
  color: lightsteelblue;
  font-family: 'Courier New', Courier, monospace;
`;

const LayoutContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  gap: 4.8rem;
  width: 100%;
  min-width: 400px;
  z-index: 1;
`;

const App = () => {
  useEffect(() => {
    function zoomOutMobile() {
      var viewport: any = document.querySelector('meta[name="viewport"]');

      if (viewport) {
        viewport.content = 'initial-scale=0.1';
        viewport.content = 'width=400'; // zoom fit for default medium difficulty
      }
    }

    zoomOutMobile();
  }, []);

  return (
    <StyledApp>
      <TwinklingStarsBackground />
      <LayoutContainer>
        <Board header={<StyledHeader>Minesweeper</StyledHeader>} />
      </LayoutContainer>
    </StyledApp>
  );
};

export { App };
