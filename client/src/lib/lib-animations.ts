import { css } from 'styled-components';

const fadeIn = css`
  animation: fadeIn 1s ease-in;
  -webkit-animation: fadeIn 1s ease-in;
  -moz-animation: fadeIn 1s ease-in;
  -o-animation: fadeIn 1s ease-in;
  -ms-animation: fadeIn 1s ease-in;

  @keyframes fadeIn {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }

  @-moz-keyframes fadeIn {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }

  @-webkit-keyframes fadeIn {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }

  @-o-keyframes fadeIn {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }

  @-ms-keyframes fadeIn {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }
`;

export { fadeIn };
