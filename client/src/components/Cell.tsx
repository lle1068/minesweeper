import { useRef, MouseEvent, useEffect, useState } from 'react';
import styled from 'styled-components';
import { fadeIn } from '../lib/lib-animations';

export type CellType = number | 'x';

type StyledCellProps = Pick<
  CellProps,
  'isRevealed' | 'cellType' | 'isFlagged'
> & {
  isRevealedMine: boolean;
  correctlyFlagged: boolean;
};

const StyledCell = styled.button<StyledCellProps>`
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: ${({ isRevealed, isRevealedMine, correctlyFlagged }) => {
    if (correctlyFlagged) {
      return 'lightgreen';
    }

    if (isRevealedMine) {
      return 'red';
    }

    if (isRevealed) {
      return 'transparent';
    }

    return 'rgba(255,255,255,0.1)';
  }};
  border-color: darkgray;
  border-width: 2px;
  border-style: ${({ isRevealed }) => isRevealed && 'dotted'};
  width: 3.2rem;
  height: 3.2rem;
  font-weight: 600;
  color: ${({ cellType, isRevealed, isFlagged }) => {
    if (isFlagged && !isRevealed) {
      return 'inherit';
    }

    if (!isRevealed) {
      return 'transparent';
    }

    switch (cellType) {
      case 1:
      case 5:
        return 'blue';
      case 2:
      case 6:
        return 'green';
      case 3:
      case 7:
        return 'red';
      case 4:
      case 8:
        return 'purple';
      case 'x':
        return 'inherit';
      default:
        return 'transparent';
    }
  }};

  ${fadeIn}
`;

type CellProps = {
  index: number;
  isGameover: boolean;
  isRevealed: boolean | undefined;
  isFlagged: boolean | undefined;
  onClick: (event: MouseEvent<HTMLButtonElement>, index: number) => void;
  isFlagMode: boolean;
  cellType: CellType;
};

const Cell = ({
  index,
  isGameover,
  isRevealed,
  isFlagged = false,
  isFlagMode,
  cellType,
  onClick
}: CellProps) => {
  const cellRef = useRef<HTMLButtonElement>(null);
  const [isRevealedMine, setIsRevealedMine] = useState(false);

  useEffect(() => {
    const preventContextMenu = (event: any) => {
      event.preventDefault();
      event.stopPropagation();
    };

    cellRef.current?.addEventListener('contextmenu', preventContextMenu);
    return () =>
      cellRef.current?.removeEventListener('contextmenu', preventContextMenu);
  }, []);

  useEffect(() => {
    if (!isGameover) {
      setIsRevealedMine(false);
    }
  }, [isGameover]);

  const renderSymbol = () => {
    if (isGameover) {
      if (isFlagged) {
        return '🚩';
      }

      return cellType === 'x' ? '💣' : cellType;
    }

    if (isRevealed) {
      return cellType === 'x' ? '💣' : cellType;
    }

    if (isFlagged) {
      return '🚩';
    }

    return null;
  };

  return (
    <StyledCell
      ref={cellRef}
      isRevealed={isGameover || isRevealed}
      isFlagged={isFlagged}
      correctlyFlagged={isGameover && isFlagged && cellType === 'x'}
      isRevealedMine={!isFlagged && isRevealedMine}
      disabled={isGameover || isRevealed}
      cellType={cellType}
      onMouseDown={(event) => {
        if (isFlagMode) {
          onClick(event, index);
          return;
        }

        if (event.button === 0 && cellType === 'x') {
          setIsRevealedMine(true);
        }

        onClick(event, index);
      }}
    >
      {renderSymbol()}
    </StyledCell>
  );
};

export { Cell };
