import {
  useRef,
  useState,
  useEffect,
  useCallback,
  useMemo,
  MouseEvent,
  ChangeEvent
} from 'react';
import styled, { css } from 'styled-components';
import swal from 'sweetalert';
import { MobileView, isMobile } from 'react-device-detect';
import { Cell, CellType } from './Cell';
import { Spinner } from './Spinner';

type Difficulty = 'easy' | 'medium' | 'hard';

type MinesweeperBoard = CellType[][];

type MinesweeperConfiguration = {
  rows: number;
  columns: number;
  numberOfMines: number;
};

type BoardProps = {
  header: JSX.Element;
};

const StyledBoard = styled.div<
  Pick<MinesweeperConfiguration, 'columns' | 'rows'> & {
    isLoading: boolean;
  }
>`
  width: ${({ columns }) => `calc(${columns} * 3.2rem)`};
  height: ${({ rows }) => `calc(${rows} * 3.2rem)`};
  border: 2px solid rgba(169, 169, 169, 0.2);

  ${({ rows, columns, isLoading }) => {
    if (isLoading) {
      return css`
        display: flex;
        align-items: center;
        justify-content: center;
      `;
    }

    return css`
      display: grid;
      grid-template-columns: repeat(${columns}, 1fr);
      grid-template-rows: repeat(${rows}, 1fr);
    `;
  }}
`;

const ResetButton = styled.button`
  color: white;
  background-color: transparent;
  font-size: 1.6rem;
`;

const StyledSelect = styled.select`
  color: white;
  background-color: transparent;
  text-align: center;
  font-size: 1.6rem;
  border-style: outset;
  border-width: 2px;
`;

const Controls = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-end;
  justify-content: space-between;
  margin-bottom: 0.6rem;
`;

const LeftControls = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  gap: 1rem;
`;

const FlagButton = styled.button<{ isFlagMode?: boolean }>`
  background-color: ${({ isFlagMode }) =>
    isFlagMode ? 'rgba(255, 255, 255, 0.1)' : 'transparent'};
  width: 2.4rem;
  height: 2.4rem;
  display: flex;
  justify-content: center;
  align-items: center;
  border-style: ${({ isFlagMode }) => isFlagMode && 'inset'};
`;

const Board = ({ header }: BoardProps) => {
  const loadingTimerRef = useRef<NodeJS.Timeout | null>(null);
  const selectDifficultyRef = useRef<HTMLSelectElement>(null);

  const [difficulty, setDifficulty] = useState<Difficulty>('medium');
  const [board, setBoard] = useState<MinesweeperBoard>();
  const [revealed, setRevealed] = useState<boolean[][]>();
  const [flagged, setFlagged] = useState<boolean[][]>();
  const [isLoading, setIsLoading] = useState(true);
  const [isGameover, setIsGameOver] = useState(false);
  const [numberFlagged, setNumberFlagged] = useState(0);
  const [numberRevealed, setNumberRevealed] = useState(0);
  const [isFlagMode, setIsFlagMode] = useState(false); // mobile only

  const config: MinesweeperConfiguration = useMemo(() => {
    switch (difficulty) {
      case 'easy':
        return { rows: 10, columns: 10, numberOfMines: 10 };
      case 'medium':
        return { rows: 16, columns: 16, numberOfMines: 40 };
      case 'hard':
        return { rows: 16, columns: 32, numberOfMines: 80 };
      default:
        // medium
        return { rows: 16, columns: 16, numberOfMines: 40 };
    }
  }, [difficulty]);

  const { rows, columns, numberOfMines } = config;

  const convertCoordToIndex = (row: number, column: number) =>
    row * columns + column;

  const convertIndexToCoord = (index: number) => {
    const row = Math.floor(index / columns);
    const column = Math.floor(index % columns);

    return {
      row,
      column
    };
  };

  const createGrid = () => Array.from(Array(rows), () => Array(columns));

  const isWithinBounds = (row: number, column: number) =>
    row >= 0 && row < rows && column >= 0 && column < columns;

  const getRandomInt = (min: number, max: number) =>
    Math.floor(Math.random() * (max - min) + min);

  const fillGridWithMines = (grid: MinesweeperBoard) => {
    let minesPlaced = 0;

    while (minesPlaced < numberOfMines) {
      const row = getRandomInt(0, rows);
      const col = getRandomInt(0, columns);

      if (grid[row][col] !== 'x') {
        grid[row][col] = 'x';
        ++minesPlaced;
      }
    }
  };

  const fillGridWithNumbers = (grid: MinesweeperBoard) => {
    const countMines = (row: number, col: number): CellType => {
      let numMines = 0;

      const x = col - 1;
      const y = row - 1;

      for (let i = y; i < y + 3; i++) {
        for (let j = x; j < x + 3; j++) {
          const isStartingPosition = i === row && j === col;

          if (!isWithinBounds(i, j) || isStartingPosition) {
            continue;
          }

          if (grid[i][j] === 'x') {
            ++numMines;
          }
        }
      }

      return numMines;
    };

    for (let i = 0; i < grid.length; i++) {
      for (let j = 0; j < grid[i].length; j++) {
        if (grid[i][j] !== 'x') {
          grid[i][j] = countMines(i, j);
        }
      }
    }
  };

  const setupGame = () => {
    const grid = createGrid();
    fillGridWithMines(grid);
    fillGridWithNumbers(grid);
    setBoard(grid);

    setRevealed(createGrid());
    setFlagged(createGrid());

    setIsLoading(false);
  };

  const resetGame = () => {
    setIsLoading(true);
    setIsGameOver(false);
    setNumberFlagged(0);
    setNumberRevealed(0);

    loadingTimerRef.current = setTimeout(() => {
      setupGame();
    }, 1500);
  };

  // setup game
  useEffect(() => {
    setupGame();

    return () => {
      clearTimeout(loadingTimerRef.current as NodeJS.Timeout);
    };
  }, []);

  // winning condition
  useEffect(() => {
    const showWinningMessage = async () => {
      await swal({
        text: 'Amazing! You won!',
        className: 'swal-alert'
      });
    };

    if (
      numberRevealed === rows * columns - numberOfMines &&
      numberFlagged === numberOfMines
    ) {
      setIsGameOver(true);
      showWinningMessage();
    }
  }, [numberFlagged, numberRevealed, numberOfMines]);

  // user updated difficulty
  useEffect(() => {
    resetGame();
  }, [difficulty]);

  const bfsReveal = (
    updatedRevealGrid: boolean[][],
    row: number,
    col: number
  ) => {
    type Coordinate = {
      row: number;
      col: number;
    };

    // used to navigate around the cell
    const navPositions: Coordinate[] = [
      {
        row: 1,
        col: -1
      },
      {
        row: 1,
        col: 0
      },
      {
        row: 1,
        col: 1
      },
      {
        row: 0,
        col: 1
      },
      {
        row: -1,
        col: 1
      },
      {
        row: -1,
        col: 0
      },
      {
        row: -1,
        col: -1
      },
      {
        row: 0,
        col: -1
      }
    ];

    // Stores coordinates of the revealed cells
    const q: Coordinate[] = [];

    // Mark the starting cell as revealed and push it into the queue
    q.push({ row, col });
    updatedRevealGrid[row][col] = true;

    let revealCount = numberRevealed + 1; // at 1 initially for current cell

    // Loop while the queue is not empty
    while (q.length > 0) {
      const cell = q[0];
      const x = cell.col;
      const y = cell.row;

      q.shift();

      // Search adjacent cells
      for (let i = 0; i < navPositions.length; i++) {
        const adjx = x + navPositions[i].col;
        const adjy = y + navPositions[i].row;

        if (isWithinBounds(adjy, adjx) && !updatedRevealGrid[adjy][adjx]) {
          updatedRevealGrid[adjy][adjx] = true;
          ++revealCount;

          if (board?.[adjy][adjx] === 0) {
            q.push({ row: adjy, col: adjx });
          }
        }
      }
    }

    setNumberRevealed(revealCount);
  };

  const handleClick = (event: MouseEvent<HTMLButtonElement>, index: number) => {
    if (!revealed || !flagged || !board) {
      return;
    }

    const { row, column } = convertIndexToCoord(index);

    // mobile only
    if (isFlagMode && isMobile) {
      const updatedFlaggedGrid = [...flagged];
      updatedFlaggedGrid[row][column] = !flagged[row][column];
      setFlagged(updatedFlaggedGrid);

      const flag = updatedFlaggedGrid[row][column];
      if (flag && board[row][column] === 'x') {
        setNumberFlagged(numberFlagged + 1);
      } else {
        setNumberFlagged(numberFlagged - 1);
      }

      return;
    }

    if (event.button === 0) {
      // left mouse button
      const updatedRevealGrid = [...revealed];
      updatedRevealGrid[row][column] = true;

      if (board[row][column] === 'x') {
        setIsGameOver(true);
        setTimeout(() => {
          swal({
            text: 'You lost! Game over!',
            className: 'swal-alert'
          });
        }, 0);
      } else {
        if (board[row][column] === 0) {
          bfsReveal(updatedRevealGrid, row, column);
        } else {
          setNumberRevealed(numberRevealed + 1);
        }
      }

      setRevealed(updatedRevealGrid);
    } else if ((event.button = 2)) {
      // right mouse button
      const updatedFlaggedGrid = [...flagged];
      updatedFlaggedGrid[row][column] = !flagged[row][column];
      setFlagged(updatedFlaggedGrid);

      const flag = updatedFlaggedGrid[row][column];
      setNumberFlagged(numberFlagged + (flag ? 1 : -1));
    }
  };

  const handleUpdateDifficulty = async (
    event: ChangeEvent<HTMLSelectElement>
  ) => {
    const yes = await swal({
      text: 'Do you want to update difficulty and reset?',
      buttons: {
        cancel: true,
        confirm: {
          text: 'Confirm'
        }
      },
      className: 'swal-alert'
    });

    setIsLoading(true);

    if (yes) {
      setDifficulty(event.target.value as Difficulty);
    } else {
      if (selectDifficultyRef.current) {
        selectDifficultyRef.current.value = difficulty;
      }

      setIsLoading(false);
    }
  };

  const handleResetButtonClick = async () => {
    if (!isGameover) {
      const yes = await swal({
        text: 'Are you sure you want to reset?',
        buttons: {
          cancel: true,
          confirm: {
            text: 'Confirm'
          }
        },
        className: 'swal-alert'
      });

      if (yes) {
        resetGame();
      }
    } else {
      resetGame();
    }
  };

  const renderCells = useCallback(() => {
    if (isLoading || !board || !revealed || !flagged) {
      return <Spinner />;
    }

    const cellGrid = Array(rows * columns);

    for (let i = 0; i < board.length; i++) {
      for (let j = 0; j < board[i].length; j++) {
        const cell = board[i][j];
        const index = convertCoordToIndex(i, j);
        cellGrid[index] = (
          <Cell
            key={index}
            index={index}
            isGameover={isGameover}
            isRevealed={revealed[i][j]}
            isFlagged={flagged[i][j]}
            cellType={cell}
            isFlagMode={isFlagMode}
            onClick={handleClick}
          />
        );
      }
    }

    return cellGrid;
  }, [board, revealed, flagged, isGameover, isLoading, isFlagMode]);

  return (
    <>
      {header}
      <div>
        <Controls>
          <LeftControls>
            <StyledSelect
              ref={selectDifficultyRef}
              disabled={isLoading}
              name="difficulty"
              defaultValue={difficulty}
              onChange={handleUpdateDifficulty}
            >
              <option value="easy">Easy</option>
              <option value="medium">Medium</option>
              <option value="hard">Hard</option>
            </StyledSelect>
            <MobileView>
              <FlagButton
                isFlagMode={isFlagMode}
                onClick={() => setIsFlagMode(!isFlagMode)}
              >
                🚩
              </FlagButton>
            </MobileView>
          </LeftControls>
          <ResetButton onClick={handleResetButtonClick}>Reset</ResetButton>
        </Controls>
        <StyledBoard rows={rows} columns={columns} isLoading={isLoading}>
          {renderCells()}
        </StyledBoard>
      </div>
    </>
  );
};

export { Board };
