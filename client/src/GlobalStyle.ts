import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
  /* prevent default browser styling to be added to elements */
  *,
  *:after,
  *:before {
    margin: 0;
    padding: 0;
    box-sizing: inherit;
  }

  /* Allows easy conversion of px to REM */
  /* Normally 1 rem is 16px, but now 1 rem is 10px */
  html {
    font-size: 62.5%;
  }

  body {
    margin: 0;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
        'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
        sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    box-sizing: border-box; /* prevents margin and padding from being added to total width or height */
  }

  .swal-alert {
    background-color: rgba(0,0,0,0.8);
    border: 1px solid lightseagreen;

    .swal-text {
      color: white;
    }
    
    .swal-button--confirm {
      background-color: lightseagreen;
    }
  }
`;
